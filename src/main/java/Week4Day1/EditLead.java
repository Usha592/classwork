package Week4Day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class EditLead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("http://leaftaps.com/opentaps");
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click(); 
		
		driver.findElementByLinkText("CRM/SFA").click();
		
		driver.findElementByLinkText("Leads").click();
		
		driver.findElementByLinkText("Find Leads").click();
		
		Thread.sleep(3000);
		
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Usha");
		
		Thread.sleep(3000);
		
		driver.findElementByLinkText("Find Leads").click();
		
		Thread.sleep(2000);
		
		//driver.findElementByXPath("//div[@cass= 'x-grid3-cell-inner x-grid3-col-firstName']").click();
		
		//driver.findElementByXPath(("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]")).click();
		
		driver.findElementByXPath("(//tbody/tr/td/div/a)[1]").click();

		Thread.sleep(2000);
		
		
		driver.getTitle();
		driver.findElementByLinkText("Edit").click();

		//driver.findElementByLinkText("Edit").click();
		
		driver.findElementById("updateLeadForm_companyName").clear();
		
		driver.findElementById("updateLeadForm_companyName").sendKeys("Amazon");
		
		driver.findElementByXPath("(//input[@type='submit'])[1]").click();
	}

}
