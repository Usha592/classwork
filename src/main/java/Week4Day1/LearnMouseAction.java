package Week4Day1;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;



public class LearnMouseAction {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("http://jqueryui.com/draggable");
		
		driver.manage().window().maximize();
		
		WebElement Frame = driver.findElementByClassName("demo-frame");
		driver.switchTo().frame(Frame);
		Actions builder  = new Actions(driver);
		
		WebElement drag = driver.findElementById("draggable");
		
		Point p = drag.getLocation();
		
		System.out.println(p);
		 int xOffset = p.x;
		 int yOffset = p.y;
		
		
		 builder.dragAndDropBy(drag, xOffset+100, yOffset+100).perform();
		
		
		
	}
	
	

}
