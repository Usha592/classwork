package Week4Day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("http://leaftaps.com/opentaps");
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click(); 
		
		driver.findElementByLinkText("CRM/SFA").click();
		
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("IBM");
		driver.findElementById("createLeadForm_firstName").sendKeys("Usha");
		driver.findElementById("createLeadForm_lastName").sendKeys("Bala");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Ush");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Bal");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Salutation");
		
		Select source = new Select(driver.findElementById("createLeadForm_dataSourceId"));
		
		source.selectByIndex(4);
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Title");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("100000");
		
		Select industry = new Select(driver.findElementById("createLeadForm_industryEnumId"));
		industry.selectByIndex(8);
		
		Select ownership = new Select(driver.findElementById("createLeadForm_ownershipEnumId"));
		ownership.selectByIndex(2);
		driver.findElementById("createLeadForm_sicCode").sendKeys("10001");
		driver.findElementById("createLeadForm_description").sendKeys("Hello Usha");
		driver.findElementById("createLeadForm_importantNote").sendKeys("Important");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("29");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("12");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("100");
		driver.findElementById("createLeadForm_departmentName").sendKeys("IT");
		Select Currency = new Select (driver.findElementById("createLeadForm_currencyUomId"));
		
		Currency.selectByIndex(4);
		
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("2");
		
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("20");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Test");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.google.com");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("Chennai");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("Puzhuthivakkam");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
		
		Select Province = new Select(driver.findElementById("createLeadForm_generalStateProvinceGeoId"));
		
		Province.selectByIndex(3);
		
		Select Country = new Select(driver.findElementById("createLeadForm_generalCountryGeoId"));
		
		Country.selectByIndex(2);
		
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("12");
		
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("12");
		
		Select Market = new Select (driver.findElementById("createLeadForm_marketingCampaignId"));
		
		Market.selectByIndex(3);
		
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9698220005");
		
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("busha592@gmail.com");
		Thread.sleep(2000);
		
		driver.findElementByClassName("smallSubmit").click();
		
	
		

	}

}
