package Week4Day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("http://leaftaps.com/opentaps");
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click(); 
		
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		Thread.sleep(2000);
		
		driver.findElementByXPath("(//tbody/tr/td/a)[3]").click();
		
		Set<String> allWindows = driver.getWindowHandles();
		
		List<String> listofwindows = new ArrayList<>();
		
		listofwindows.addAll(allWindows);
		
		driver.switchTo().window(listofwindows.get(1));
		
		driver.findElementByName("id").sendKeys("10130");
		
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		Thread.sleep(2000);
		
		driver.findElementByXPath("(//tbody/tr/td/div/a)[1]").click();
		
		
		
		
		driver.switchTo().window(listofwindows.get(0));
		
		driver.findElementByXPath("(//tbody/tr/td/a)[4]").click();
		
		Set<String> allWindows1 = driver.getWindowHandles();
		
		List<String> listofwindows1 = new ArrayList<>();
		
		listofwindows1.addAll(allWindows1);
		
		driver.switchTo().window(listofwindows1.get(1));
		
		
		driver.findElementByName("id").sendKeys("10190");
		
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		Thread.sleep(2000);
		
		driver.findElementByXPath("(//tbody/tr/td/div/a)[1]").click();
		
		driver.switchTo().window(listofwindows.get(0));
		
		driver.findElementByLinkText("Merge").click();
		
		driver.switchTo().alert().accept();
		
		
		driver.findElementByLinkText("Find Leads").click();
		
		driver.findElementByName("id").sendKeys("10129");
		
		
		
		
		
		
		
	}

}
