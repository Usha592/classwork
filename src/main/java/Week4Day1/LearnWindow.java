package Week4Day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindow {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("https://irctc.co.in");
		

		driver.manage().window().maximize();
		
		driver.findElementByXPath("//span[text() ='AGENT LOGIN']").click();
		driver.findElementByLinkText("Contact Us").click();
		
		Set<String> windows = driver.getWindowHandles();
		
		List<String> winlist = new ArrayList<>();
		
		winlist.addAll(windows);
		
		driver.switchTo().window(winlist.get(1));
		
		System.out.println(driver.getCurrentUrl());	
		System.out.println(driver.getTitle());
		
		File src = driver.getScreenshotAs(OutputType.FILE);
		
		File desc = new File ("./snaps/contactus.png");
		FileUtils.copyFile(src,desc);
		
		driver.switchTo().window(winlist.get(0));
		driver.close();
	}

}
