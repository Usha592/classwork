package Week4Day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlert {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		
		driver.switchTo().frame("iframeResult");
		
		driver.findElementByXPath("//button[text() ='Try it']").click();
		
		driver.switchTo().alert();
		
		driver.switchTo().alert().sendKeys("Usha");
		
		driver.switchTo().alert().accept();
		
		String s = driver.findElementById("demo").getText();
		
		System.out.println(s);
		
		driver.switchTo().defaultContent();
		
		
		
			
			

	}

}
