package utils;

	import java.io.IOException;
	import org.apache.poi.xssf.usermodel.XSSFCell;
	import org.apache.poi.xssf.usermodel.XSSFRow;
	import org.apache.poi.xssf.usermodel.XSSFSheet;
	import org.apache.poi.xssf.usermodel.XSSFWorkbook;

	public class ReadExcel {

	    public static Object[] [] getDataExcel (String fileName) throws IOException {
	        // TODO Auto-generated method stub
	        XSSFWorkbook  wbook = new XSSFWorkbook("./Data/"+fileName+".xlsx");
	        
	        XSSFSheet sheet = wbook.getSheetAt(0);
	        
	        int rowcount = sheet.getLastRowNum();
	        
	        System.out.println("Row Count is " +rowcount);
	        
	        int columncount = sheet.getRow(0).getLastCellNum();
	        
	        System.out.println("Column Count is " +columncount);
	        
	        Object[][] data = new Object[rowcount][columncount];
	        for (int j=1;j<=rowcount;j++)
	        {
	            XSSFRow row = sheet.getRow(j);
	            
	            for (int i =0; i<columncount; i++)
	            {
	                XSSFCell cell = row.getCell(i);
	                
	                data[j-1][i]= cell.getStringCellValue();
	                
	                //String cellvalue = cell.getStringCellValue();
	                
	                System.out.println(data);  
	                
	                
	                
	            }
	        }
			return data;
	     }

	    
	}


