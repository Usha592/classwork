package Groups;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.SeMethods;
import wdMethodsnew.ProjectMethods;



public class TC1_CreateLead extends ProjectMethods {
	
	@BeforeTest(groups= {"smoke"})
	
	public void setData()
	
	{
		testCaseName ="TC1_CreateLead";
		testCaseDesc = "Create a Lead";
		
		category ="Smoke";
		author ="Usha";
		
	}
	@Test(groups= {"smoke"})
	public void CreateLead() {
		
		/*startApp("Chrome" ,"http://leaftaps.com/opentaps");
		WebElement user = locateElement("id","username");
		type(user,"DemoSalesManager");
		
		WebElement pwd = locateElement("id","password");
		type(pwd,"crmsfa");
		
		WebElement login = locateElement("class","decorativeSubmit");
		click(login);
		
		WebElement crmsfa = locateElement("linktext","CRM/SFA");
		click(crmsfa);*/
		
		
		//Test Case Steps
		
		
		
		WebElement createlead = locateElement("linkText","Create Lead");
		click(createlead);
		WebElement companyname = locateElement("id","createLeadForm_companyName");
		type(companyname ,"IBM");
		
		
		WebElement fname = locateElement("id","createLeadForm_firstName");
		type(fname,"Usha");
		
		WebElement lname =locateElement("id","createLeadForm_lastName");
		type(lname,"Bala");
		WebElement source = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingIndex(source ,3);
		
		WebElement phone = locateElement("id","createLeadForm_primaryPhoneNumber");
		
		type(phone,"9698220005");
		
		WebElement email = locateElement("id","createLeadForm_primaryEmail");
		
		type(email,"busha592@gmail.com");
		
		WebElement create = locateElement("class","smallSubmit");
		click(create);
		
	}
	@AfterSuite(groups= {"smoke"})
	public void endofreport()
	{
	endResult();
	}
	



}
