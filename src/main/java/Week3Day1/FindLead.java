package Week3Day1;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class FindLead {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			
			ChromeDriver driver = new ChromeDriver();
			
			driver.get("http://leaftaps.com/opentaps");
			
			driver.manage().window().maximize();
			
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			driver.findElementById("username").sendKeys("DemoSalesManager");
			
			driver.findElementById("password").sendKeys("crmsfa");
			
			driver.findElementByClassName("decorativeSubmit").click(); 
			
			driver.findElementByLinkText("CRM/SFA").click();
			
			driver.findElementByLinkText("Create Lead").click();
			
			driver.findElementByLinkText("Find Leads").click();
			
			driver.findElementById("ext-gen246").sendKeys("11021");
			
			driver.findElementById("ext-gen248").sendKeys("Usha");
			
			driver.findElementById("ext-gen250").sendKeys("B");
			
			driver.findElementById("ext-gen252").sendKeys("IBM");
			
			driver.findElementById("ext-gen334").click();
			
			
		} 
		
		
		catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Unable to find the Locator");
			
			
		}
		
}

}
