package Week3Day1;

import java.util.*;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("http://leaftaps.com/opentaps");
		
		driver.manage().window().maximize();
		
		driver.findElementById("username").sendKeys("DemoSalesManager");
		
		driver.findElementById("password").sendKeys("crmsfa");
		
		driver.findElementByClassName("decorativeSubmit").click(); 
		
		driver.findElementByLinkText("CRM/SFA").click();
		
		driver.findElementByLinkText("Create Lead").click();
		
	    driver.findElementById("createLeadForm_companyName").sendKeys("IBM");
		
		driver.findElementById("createLeadForm_firstName").sendKeys("Usha");
		
		driver.findElementById("createLeadForm_lastName").sendKeys("Balasubramanian");
		
		//driver.findElementByClassName("smallSubmit").click();
		
		WebElement src = driver.findElementById("createLeadForm_dataSourceId");
		
		Select dropdown = new Select(src);
		
		dropdown.selectByVisibleText("Direct Mail");
		
		WebElement mrkt = driver.findElementById("createLeadForm_marketingCampaignId");
		
		Select drop = new Select(mrkt);
		
		drop.selectByIndex(7);
		
		/*List<WebElement> source = dropdown.getOptions();
		
		//Print Options in Source Dropdown
				
		for (WebElement eachoption :source) {
			
			System.out.println(eachoption.getText());
			
		}
		
		List<WebElement> marketing = drop.getOptions();
		
		// Print  Options in Marketing Campaign DropDown
		for (WebElement eachopt : marketing) {
			
			System.out.println(eachopt.getText());
			
		} */
		

		driver.findElementByClassName("smallSubmit").click();
		
		
		
		}

}
