package Week3Day2;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFindElementsandWebTable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("http://leafground.com/pages/table.html");

		driver.manage().window().maximize();
		
		List<WebElement> check = driver.findElementsByXPath("//input[@type='checkbox']");
		System.out.println(check.size());
		
		
		WebElement ele = check.get(2); 
		
		ele.click();
		
		WebElement table = driver.findElementByXPath("//tr[@class='even']/td[3]");
		
		table.click();	
		
		
		
		
		
		
		}

}
