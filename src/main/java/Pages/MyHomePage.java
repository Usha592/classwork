package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethodsnew.ProjectMethods;

public class MyHomePage extends ProjectMethods {
	
	public MyHomePage()
	{
		PageFactory.initElements(driver, this);
	}

	@FindBy (linkText = "Create Lead")
	WebElement eleCreateLead ;
	
	public void myHome()
	
	{
		
		click(eleCreateLead);
		
	}
}
