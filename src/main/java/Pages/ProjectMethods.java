package Pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import week6Day2.ReadExcel;

public class ProjectMethods extends SeMethods{
	@BeforeSuite(groups= {"smoke"})
	public void beforeSuite() {
		beginResult();
	}
	@BeforeClass(groups= {"smoke"})
	public void beforeClass() {
		startTestCase();
	}
	@Parameters({"url"})
	@BeforeMethod(groups= {"smoke"})
	public void login(String url) {
		startApp("chrome", url);
		
	}
	@AfterMethod(groups= {"smoke"})
	public void closeApp() {
		closeBrowser();
	}
	@AfterSuite(groups= {"smoke"})
	public void afterSuite() {
		endResult();
	}
	
	@DataProvider(name = "fetchData")
	 
	 public Object[][] fetchData() throws IOException
	 
	 {
		 	 
		return ReadExcel.getDataExcel(excelfilename);
		
	 }

	
	
	
	
	
	
	
	
	
}
