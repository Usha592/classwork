package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethodsnew.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {
	
	public ViewLeadPage()
	{
		PageFactory.initElements(driver, this);
	}

	@FindBy (id = "viewLead_firstName_sp")
	WebElement eleverifyfName ;
	
	public ViewLeadPage viewLead(String data)
	
	{
		
		
		verifyExactText(eleverifyfName,data);
		return this;
		
	}
	
	@FindBy (linkText = "Edit")
	WebElement eleEdit;
	
	public ViewLeadPage editLead()
	{
		click(eleEdit);
		
		return this;
	}
	
	@FindBy(id = "updateLeadForm_companyName")
	WebElement eleUpdatecName;
	
	public ViewLeadPage updateCname (String data)
	{
		type(eleUpdatecName,data);
		return this;
		
		
	}
	
	@FindBy (how =How.XPATH, using = "(//input[@type='submit'])[1]")
	WebElement eleUpdate;
	
	public ViewLeadPage update()
	{
		click(eleUpdate);
		
		return this;
	}
	
	@FindBy(linkText= "Logout")
	WebElement eleLogout;
	public HomePage Logout ()
	
	{
		click(eleLogout);
		return new  HomePage();
		
	}
}

