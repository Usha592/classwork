

package Pages;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethodsnew.ProjectMethods;



	
	public class CreateLead extends ProjectMethods {
		
		@BeforeTest
		
		public void setData()
		
		{
			testCaseName ="TC1_CreateLead";
			testCaseDesc = "Create a Lead";
			
			category ="Smoke";
			author ="Usha";
			excelfilename ="Data";
		}
		@Test(dataProvider="fetchData")
		public void Create (String uname, String pwd,String cName ,String fName ,String lName,String vFname)
		
		{
			
			new LoginPage().typeUserName(uname).typePassword(pwd).clickLogin();
			
			new HomePage().clickCrmSFA();
			
			new MyHomePage().myHome();
			
			new CreateLeadPage().typeCName(cName).typefName(fName).typelName(lName).createLead();
			
			new ViewLeadPage().viewLead(vFname).Logout();
			
			
		}
		
		
	 
	}

	


