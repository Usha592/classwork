package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethodsnew.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {
	
	public CreateLeadPage()
	{
		PageFactory.initElements(driver, this);
	}

	@FindBy (id  = "createLeadForm_companyName")
	WebElement elecName ;
	
	public CreateLeadPage typeCName(String data)
	{
		type(elecName,data);
		return this;
		
		
	}
	
	@FindBy (id = "createLeadForm_firstName")
	WebElement elefName;
	
	public CreateLeadPage typefName(String data)
	{
		type(elefName,data);
		
		return this;
		
	}
	
	@FindBy(id= "createLeadForm_lastName")
	WebElement elelName;
	
	public CreateLeadPage typelName(String data)
	{
		type(elelName,data);
		return this;
	}
	
	@FindBy(how =How.CLASS_NAME ,using = "smallSubmit")
	WebElement eleCreateLead;
	
	public ViewLeadPage createLead()
	
	{
		
		click(eleCreateLead);
		return new ViewLeadPage();
		
	}
}
