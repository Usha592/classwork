package Week4Day2ProjectDay;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class TC1_FacebookPage {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		driver = new ChromeDriver(options);
		
		driver.get("http://www.facebook.com");
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.findElementById("email").sendKeys("banupriya817@gmail.com");
		
		driver.findElementById("pass").sendKeys("geethapriya");
		
		driver.findElementByXPath("//input[@value ='Log In']").click();
		
		Thread.sleep(2000);
		
		driver.findElementByXPath("//input[@data-testid='search_input']").sendKeys("TestLeaf");
		
		Thread.sleep(2000);
		
		driver.findElementByXPath("//button[@data-testid='facebar_search_button']").click();
		
		Thread.sleep(5000);
		WebElement tf = driver.findElementByXPath("//span[text()='Pages']/following::div[text()='TestLeaf']");
		
		String s = tf.getText();
		
		System.out.println(s);
		
		String buttonVal = driver.findElementByXPath("(//span[text()='Pages']/following::button[@type='submit'])[1]").getText();
		
		System.out.println(buttonVal);
		
		String a = "Liked";
		
		if(a==buttonVal)
			
		{
			
			System.out.println("The TestLeaf Page is Already Liked");
		}
		
		driver.findElementByXPath("//span[text()='Pages']/following::div[text()='TestLeaf']").click();
		
		String likes =driver.findElementByXPath("//span[text()='Community']/following::div[text()='6,713 people like this']").getText();
		
		System.out.println(likes);
		}
		
		
		
	}

