package ParamaterandDataProvider;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.SeMethods;
import wdMethodsnew.ProjectMethods;
import week6Day2.ReadExcel;



public class TC1_CreateLead extends ProjectMethods {
	
	@BeforeTest
	
	public void setData()
	
	{
		testCaseName ="TC1_CreateLead";
		testCaseDesc = "Create a Lead";
		
		category ="Smoke";
		author ="Usha";
		excelfilename  ="CreateLead";
	}
	@Test(dataProvider="fetchData")
	public void CreateLead (String cName ,String fName ,String lName)
	
	{
		
		//Test Case Steps
		
		
		
		WebElement createlead = locateElement("linkText","Create Lead");
		click(createlead);
		WebElement companyname = locateElement("id","createLeadForm_companyName");
		type(companyname ,cName);
		
		
		WebElement fname = locateElement("id","createLeadForm_firstName");
		type(fname,fName);
		
		WebElement lname =locateElement("id","createLeadForm_lastName");
		type(lname,lName);
		WebElement source = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingIndex(source ,3);
		
		WebElement phone = locateElement("id","createLeadForm_primaryPhoneNumber");
		
		type(phone,"9698220005");
		
		WebElement email = locateElement("id","createLeadForm_primaryEmail");
		
		type(email,"busha592@gmail.com");
		
		WebElement create = locateElement("class","smallSubmit");
		click(create);
		
	}
	
	
 
}

