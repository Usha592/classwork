package ParamaterandDataProvider;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.SeMethods;
import wdMethodsnew.ProjectMethods;


public class TC2_EditLead extends ProjectMethods{
	
	
	@BeforeTest(groups= {"sanity"},dependsOnGroups= {"smoke"})
	
	public void setData()
	
	{
		testCaseName ="TC2_EditLead";
		testCaseDesc = "Create a Lead";
		
		category ="Smoke";
		author ="Usha";
		
	}

		// TODO Auto-generated method stub
	@Test(groups= {"sanity"})
		public void editlead()
		{
		
		/*startApp("Chrome" ,"http://leaftaps.com/opentaps");
		WebElement user = locateElement("id","username");
		type(user,"DemoSalesManager");
		
		WebElement pwd = locateElement("id","password");
		type(pwd,"crmsfa");
		
		WebElement login = locateElement("class","decorativeSubmit");
		click(login);
		
		WebElement crmsfa = locateElement("linktext","CRM/SFA");
		click(crmsfa);*/
		
		WebElement leads = locateElement("linkText","Leads");
		click(leads);
		
		WebElement findleads = locateElement("linkText","Find Leads");
		click(findleads);
		
		WebElement fname = locateElement("xpath","(//input[@name='firstName'])[3]");
		type(fname,"Usha");
		
		WebElement findleads1 = locateElement("linkText","Find Leads");
		click(findleads1);
		
		WebElement firstid = locateElement("xpath","(//tbody/tr/td/div/a)[1]");
		click(firstid);
		
		WebElement edit = locateElement("linkText","Edit");
		click(edit);
		
		WebElement companyname = locateElement("id","updateLeadForm_companyName");
		type(companyname,"Amazon");
		
		WebElement update = locateElement("xpath","(//input[@type='submit'])[1]");
		click(update);
						
				
		}
	
	@AfterSuite(groups= {"sanity"})
	public void endofreport()
	{
	endResult();
	}
	}


