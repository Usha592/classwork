package Week4Day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethodsnew.ProjectMethods;

public class TC5_MergeLead extends ProjectMethods{

	@BeforeTest
		
		public void setData()
		
		{
			testCaseName ="TC5_MergeLead";
			testCaseDesc = "Merge Lead";
			
			category ="Smoke";
			author ="Usha";
			
		}
	
	@Test
	public void CreateLead() throws InterruptedException {
		
		WebElement leads = locateElement("linkText","Leads");
		click(leads);
		
		WebElement mergeleads = locateElement("linkText" ,"Merge Leads");
		click(mergeleads);
		
		Thread.sleep(2000);
		
		WebElement fromlead = locateElement("xpath" ,"(//tbody/tr/td/a)[3]");
		click(fromlead);
		
		switchToWindow(1);
		
		WebElement fromleadid = locateElement("name" ,"id");
		type(fromleadid ,"10137");
		
		
		WebElement findresults1 = locateElement("xpath" ,"//button[text()='Find Leads']");
		click(findresults1);
		
		Thread.sleep(2000);
		
		WebElement results1  = locateElement("xpath" ,"(//tbody/tr/td/div/a)[1]");
		click(results1);
		
		switchToWindow(0);
		
		
		WebElement tolead = locateElement("xpath" ,"(//tbody/tr/td/a)[4]");
		click(tolead);
		
		switchToWindow(1);
		
		WebElement toleadid = locateElement("name" ,"id");
		type(toleadid ,"10124");
		WebElement findresults2 = locateElement("xpath" ,"//button[text()='Find Leads']");
		click(findresults2);
		
		Thread.sleep(2000);
		WebElement results2  = locateElement("xpath" ,"(//tbody/tr/td/div/a)[1]");
		click(results2);
		
		switchToWindow(0);
		
		WebElement merge =locateElement("linkText" ,"Merge");
		click(merge);
		
		acceptAlert();
		
		
		
		
		
		
		
		
	}
	
	@AfterSuite
	public void endofreport()
	{
	endResult();
	}

	

	}


