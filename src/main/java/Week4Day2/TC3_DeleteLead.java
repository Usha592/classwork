package Week4Day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.SeMethods;
import wdMethodsnew.ProjectMethods;


public class TC3_DeleteLead extends ProjectMethods{
	
	
	@BeforeTest
	
	public void setData()
	
	{
		testCaseName ="TC3_DeleteLead";
		testCaseDesc = "Delete a Lead";
		
		category ="Smoke";
		author ="Usha";
		
	}

		// TODO Auto-generated method stub
	@Test
	
	//(dependsOnMethods = {"Week4Day2.TC1_CreateLead.CreateLead"})
		public void deletelead()
		{
		
		
		
		WebElement leads = locateElement("linkText","Leads");
		click(leads);
		
		WebElement findleads = locateElement("linkText","Find Leads");
		click(findleads);
		
		WebElement fname = locateElement("xpath","(//input[@name='firstName'])[3]");
		type(fname,"Usha");
		
		WebElement findleads1 = locateElement("linkText","Find Leads");
		click(findleads1);
		
		WebElement firstid = locateElement("xpath","(//tbody/tr/td/div/a)[1]");
		click(firstid);
		
		WebElement edit = locateElement("linkText","Delete");
		click(edit);
		
						
				
		}
	
	@AfterSuite
	public void endofreport()
	{
	endResult();
	}
	}


