package Week4Day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethodsnew.ProjectMethods;

public class TC4_DuplicateLead extends ProjectMethods{

	@BeforeTest
		
		public void setData()
		
		{
			testCaseName ="TC4_DuplicateLead";
			testCaseDesc = "Duplicate Lead";
			
			category ="Smoke";
			author ="Usha";
			
		}
	
	@Test
	public void CreateLead() {
		
		WebElement leads = locateElement("linkText","Leads");
		click(leads);
		
		WebElement findleads = locateElement("linkText","Find Leads");
		click(findleads);
		
		WebElement Email = locateElement ("xpath","//span[text()='Email']");
		click(Email);
		
		WebElement emailenter = locateElement ("name","emailAddress");
		type(emailenter,"busha592@gmail.com" );
		
		
		WebElement findleads1 = locateElement("linkText","Find Leads");
		click(findleads1);
		
		WebElement firstid = locateElement("xpath","(//tbody/tr/td/div/a)[1]");
		click(firstid);
		
		System.out.println(firstid.getText());
		
		WebElement duplicate = locateElement ("linkText", "Duplicate Lead");
		click(duplicate);
		
		driver.getTitle();
		
		
		
		
		
	}
	
	@AfterSuite
	public void endofreport()
	{
	endResult();
	}

	

	}


