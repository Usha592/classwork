package CodingChallenge;

import java.util.Scanner;

public class ReverseNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner (System.in);
		
		System.out.println("Enter a number");
		
		int num1 = input.nextInt();
		
			
		int temp=0;
		
		
		while(num1 != 0)
	      {
	          temp = temp * 10;
	          temp = temp + num1%10;
	          num1 = num1/10;
	      }
		
		System.out.println("Reverse of Given Number is " +temp);
		
		
		

	}

}
