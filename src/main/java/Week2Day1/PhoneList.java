package Week2Day1;

import java.awt.List;
import java.util.ArrayList;
import java.util.Collections;

public class PhoneList {

	//private static final String Phonelist = null;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ArrayList<String> PhoneList = new ArrayList<String>();
		
		
		PhoneList.add("Nokia");
		PhoneList.add("Samsung");
		PhoneList.add("Samsung");
		PhoneList.add("Moto");
		int size  =  PhoneList.size();

		
		// Print List of Phones
		System.out.println(" No of Phones Used is  :" + size);
		
		// Print Last But One Phone
		
		System.out.println(" Last But One Phone is :" +PhoneList.get(size-2) );
		
		
		// Before Sorting
		
		System.out.println(" Before Sorting");
		
		for(String counter: PhoneList){
			
			
			System.out.println(counter);
		}
		
		// Print Sort Order in ASCII Value
		
		Collections.sort(PhoneList);
		
		
		System.out.println(" After Sorting");
		
		for(String counter: PhoneList){
			
			
			System.out.println(counter);
		}

			
	}

}
