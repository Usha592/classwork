package Week5Day1;


import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReport {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/results.html");
		
		html.setAppendExisting(true);
		
		ExtentReports extent = new ExtentReports();
		
		extent.attachReporter(html);
		ExtentTest test = extent.createTest("TC_Create Lead", "Create a New Lead");
		test.assignCategory("Smoke");
		test.assignAuthor("Usha");
		test.pass("Browser Launched Succesfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.pass("Entered UserName Succesfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
		test.pass("Entered Password Succesfully");
		test.pass("Succcessfully Logged In");
		
		extent.flush();
		
		
		
		
	}

}
